package test.threading;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class ThreadingActivity extends Activity {
	ProgressBar mProgressBar;
	ImageView mImageView;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
		mProgressBar.setVisibility(View.INVISIBLE);
		
		mImageView = (ImageView)findViewById(R.id.imageView);
		mImageView.setVisibility(View.INVISIBLE);

		// download an image of 300+ kB
		new LoadImage().execute("http://www.verbeterdebuurt.nl/blog/wp-content/uploads/2011/06/Android-Applications-List.jpg");
	}

	Bitmap downloadImage(String fileUrl){
		Bitmap bitmap = null;
		URL url = null;          
		try {
			url = new URL(fileUrl);
			HttpURLConnection conn= (HttpURLConnection)url.openConnection();
			conn.setDoInput(true);
			conn.connect();
			InputStream is = conn.getInputStream();

			bitmap = BitmapFactory.decodeStream(is);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bitmap;
	}

	private class LoadImage extends AsyncTask<String, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... urls) {
			return downloadImage(urls[0]);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressBar.setVisibility(View.VISIBLE);
			mImageView.setVisibility(View.INVISIBLE);
		}

		@Override
		protected void onPostExecute(Bitmap bitmap) {
			super.onPostExecute(bitmap);
			mProgressBar.setVisibility(View.INVISIBLE);
			mImageView.setImageBitmap(bitmap);
			mImageView.setVisibility(View.VISIBLE);
		}

	}
}