package test.notification;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class NotificationServiceActivity extends Activity {
	NotificationManager mNotificationManager;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        // get the notification manager
        mNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        
        // cancel any previous notifications
        mNotificationManager.cancel(1);
    }
    
	public void onButtonClick(View v) {
        // create the notification
        int icon = R.drawable.ic_launcher;
        CharSequence notificationText = "Hello!";
        long now = System.currentTimeMillis();
        Notification notification = new Notification(icon, notificationText, now);
        
        // define the message
        CharSequence title = "My notification";
        CharSequence body = "Hello world!";
        Intent notificationIntent = new Intent(getApplicationContext(), NotificationServiceActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);
        notification.setLatestEventInfo(getApplicationContext(), title, body, pendingIntent);
        
        // issue the notification
        mNotificationManager.notify(1, notification);				
	}
}