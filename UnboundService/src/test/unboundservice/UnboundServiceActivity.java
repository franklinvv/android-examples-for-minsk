package test.unboundservice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class UnboundServiceActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Intent serviceIntent = new Intent(this, MyService.class);
        startService(serviceIntent);
    }
}