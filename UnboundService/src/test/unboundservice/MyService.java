package test.unboundservice;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

public class MyService extends Service {
	private Handler handler = new Handler();
	private int counter = 0;
	private Runnable task = new Runnable() {
		@Override
		public void run() {
			counter++;
			if(counter <= 3) {
				Toast.makeText(getApplicationContext(), "Toast nr " + counter, Toast.LENGTH_LONG).show();
				handler.postDelayed(task, 10*1000);
			} else {
				Toast.makeText(getApplicationContext(), "Service is stopped", Toast.LENGTH_LONG).show();
				stopSelf();
			}
		}
	};

	@Override
	public IBinder onBind(Intent arg0) {
		// return null, because we don't allow binding with this unbound service
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		handler.post(task);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

}
