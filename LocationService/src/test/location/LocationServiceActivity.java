package test.location;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;

public class LocationServiceActivity extends Activity implements LocationListener {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        // get the location manager
        LocationManager locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
        
        // request location updates at least every minTime seconds and minMeters location difference
        int minTime = 0;
        int minMeters = 0;
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, minTime, minMeters, this);
    }

	public void onLocationChanged(Location location) {
		// called if we have received a location fix
		TextView textView = (TextView)findViewById(R.id.textView);
		String locationString = String.format("Latitude: %1$f, longitude: %2$f", location.getLatitude(), location.getLongitude()); 
		textView.setText(locationString);
	}

	public void onProviderDisabled(String provider) {
		// called if the provider is disabled
	}

	public void onProviderEnabled(String provider) {
		// called if the provider is enabled
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// called if the provider status has changed
	}
}