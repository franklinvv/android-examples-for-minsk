Android example code for Minsk
==============================

This repository contains some example projects for code that we discussed during my training in Minsk.

* `AlarmService` contains example code for using internal alarms to execute code at random moments in time
* `BootReceiver` shows how to execute code after the device has booted
* `BoundService` shows an exampe of a bound service
* `IntentReceiverByCode` contains an example broadcast receiver defined in code
* `IntentReceiverByXML` shows an example broadcast receiver defined in XML in the manifest
* `LocationService` shows how to get the current location of the device
* `NotificationService` shows how to issuen notifications in the notification bar
* `Preferences` shows how to use simple user preferences defined in XML
* `SQLite` shows how to store data in a local SQL database
* `ShareIntent` shows how to share content with other applications
* `Threading` has an example of AsyncTask
* `UnboundService` shows an example of an unbound service


How to import this code
-----------------------

In Eclipse, you can import each of these project by just choosing File and then Import.