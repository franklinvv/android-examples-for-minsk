package test.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String DB_NAME = "Example";
	private static final int DB_VERSION = 1;
	
	private static final String DICT_TABLE_NAME = "dictionary";
	private static final String KEY = "key";
	private static final String VALUE = "value";
	private static final String DICT_TABLE_CREATE =
            "CREATE TABLE " + DICT_TABLE_NAME + " (" +
            KEY + " TEXT, " +
            VALUE + " TEXT);";
	
	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	// called when the database is created for the first time
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DICT_TABLE_CREATE);
	}
	
	public void setValue(String key, String value) {
		SQLiteDatabase db = getWritableDatabase();
		
		try {
			db.beginTransaction();
			
			ContentValues cv = new ContentValues();
            cv.put(KEY, key);
            cv.put(VALUE, value);
            db.insert(DICT_TABLE_NAME, null, cv);

			db.setTransactionSuccessful();
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}
	}
	
	public String getValue(String key) {
		SQLiteDatabase db = getReadableDatabase();
		String value = null;
		
		try {
			String[] columns = {VALUE};
            String[] params = {key};
            String sql = SQLiteQueryBuilder.buildQueryString(false,
                            DICT_TABLE_NAME,
                            columns,
                            KEY + " = ?", null, null, null, null);
            Cursor result = db.rawQuery(sql, params);
            result.moveToFirst(); // don't forget!
            if(result.getCount() > 0) {
            	value = result.getString(0);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return value;
	}

	// called when the database version has increased
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

}
