package test.sqlite;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class SQLiteActivity extends Activity {
	private static final int DIALOG_SET_VALUE = 1;
	private static final int DIALOG_GET_VALUE = 2;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);


	}

	public void onGetValueButtonClick(View v) {
		showDialog(DIALOG_GET_VALUE);
	}

	public void onSetValueButtonClick(View v) {
		showDialog(DIALOG_SET_VALUE);
	}

	@Override
	public Dialog onCreateDialog(int id) {
		Dialog dialog;
		Builder builder;
		LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
		switch(id) {
		case DIALOG_GET_VALUE:
			builder = new AlertDialog.Builder(this);
			builder.setTitle("Get a value");
			builder.setMessage("Enter a key");
			inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
			final LinearLayout contentForGetValue = (LinearLayout)inflater.inflate(R.layout.dialog_get_value, null);
			builder.setView(contentForGetValue);
			builder.setPositiveButton(R.string.retrieve, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					EditText etKey = (EditText)contentForGetValue.findViewById(R.id.editTextKey);
					String key = etKey.getText().toString();
					
					DatabaseHelper dbHelper = new DatabaseHelper(SQLiteActivity.this);
					String value = dbHelper.getValue(key);
					
					if(value == null) {
						value = "No value found in database!";
					}
					Toast.makeText(SQLiteActivity.this, value, Toast.LENGTH_LONG).show();
					removeDialog(DIALOG_GET_VALUE);
				}
			});
			builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					removeDialog(DIALOG_GET_VALUE);
				}
			});
			dialog = builder.create();
			return dialog;
		case DIALOG_SET_VALUE:
			builder = new AlertDialog.Builder(this);
			builder.setTitle("Set a value");
			builder.setMessage("Enter a key and a value");

			final LinearLayout contentForSetValue = (LinearLayout)inflater.inflate(R.layout.dialog_set_value, null);
			builder.setView(contentForSetValue);
			builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					EditText etKey = (EditText)contentForSetValue.findViewById(R.id.editTextKey);
					EditText etValue = (EditText)contentForSetValue.findViewById(R.id.editTextValue);
					String key = etKey.getText().toString();
					String value = etValue.getText().toString();
					
					DatabaseHelper dbHelper = new DatabaseHelper(SQLiteActivity.this);
					dbHelper.setValue(key, value);
				}
			});
			builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			dialog = builder.create();
			return dialog;
		}
		return super.onCreateDialog(id);
	}
}