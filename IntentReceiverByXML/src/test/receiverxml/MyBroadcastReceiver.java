package test.receiverxml;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MyBroadcastReceiver extends BroadcastReceiver {
	public static final String INTENT_ACTION = "test.receiverxml.INTENT_EXAMPLE_BY_XML";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if(intent.getAction().equalsIgnoreCase(INTENT_ACTION)) {
			Toast.makeText(context, "I received an intent!", Toast.LENGTH_LONG).show();	
		}
	}

}
