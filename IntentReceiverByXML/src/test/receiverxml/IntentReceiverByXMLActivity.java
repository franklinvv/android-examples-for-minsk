package test.receiverxml;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class IntentReceiverByXMLActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    
    public void onButtonClick(View v) {
    	Intent intent = new Intent();
    	intent.setAction(MyBroadcastReceiver.INTENT_ACTION);
    	sendBroadcast(intent);
    }
}