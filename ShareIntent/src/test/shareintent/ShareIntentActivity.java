package test.shareintent;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class ShareIntentActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    
    public void onButtonShareTextClick(View v) {
    	Intent intent = new Intent(Intent.ACTION_SEND);
    	
    	// this defines what type of content you want to share
    	intent.setType("text/plain");
    	
    	intent.putExtra(Intent.EXTRA_SUBJECT, "This is the subject");
    	intent.putExtra(Intent.EXTRA_TEXT, "Hello, I would like to share this text with you!");
    	startActivity(Intent.createChooser(intent, "Share your message"));
    }
    
    public void onButtonShareImageClick(View v) {
    	Intent intent = new Intent(Intent.ACTION_SEND);
    	
    	String path = "/sdcard/image.png";
    	File file = new File(path);
    	Uri imageUri = Uri.fromFile(file);
    	
    	intent.putExtra(Intent.EXTRA_STREAM, imageUri);
    	intent.setType("image/png");
    	startActivity(Intent.createChooser(intent, "Share your picture"));
    }
}