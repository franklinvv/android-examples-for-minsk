package test.receivercode;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class IntentReceiverByCodeActivity extends Activity {
	private static final String INTENT_ACTION = "test.receivercode.INTENT_EXAMPLE";
	private MyBroadcastReceiver broadcastReceiver;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		broadcastReceiver = new MyBroadcastReceiver();
		IntentFilter intentFilter = new IntentFilter(INTENT_ACTION);
		registerReceiver(broadcastReceiver, intentFilter);
	}

	public void onButtonClick(View v) {
		Intent intent = new Intent();
		intent.setAction(INTENT_ACTION);
		sendBroadcast(intent);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(broadcastReceiver);
	}

	private class MyBroadcastReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equalsIgnoreCase(INTENT_ACTION)) {
				Toast.makeText(context, "I received an intent", Toast.LENGTH_LONG).show();
			}
		}

	}
}