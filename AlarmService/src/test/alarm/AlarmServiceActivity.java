package test.alarm;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;

public class AlarmServiceActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        // set up pending intent
        Intent notificationIntent = new Intent(getApplicationContext(), AlarmServiceActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);
        
        // get the alarm manager
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        
        // define the first time the alarm should be triggered
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, 10);
        
        // create a repeating schedule
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 30 * 1000, pendingIntent);
    }
}