package test.service;

import java.util.Random;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {
	
	private final IBinder mBinder = new MyBinder();
	private final Random mRandom = new Random();
	
	public class MyBinder extends Binder {
		MyService getService() {
			return MyService.this;
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}
	
	public int getRandomInteger() {
		int randomInt = mRandom.nextInt(12345);
		Log.d("MyService", "I generated a random number: " + randomInt);
		return randomInt;
	}

}
