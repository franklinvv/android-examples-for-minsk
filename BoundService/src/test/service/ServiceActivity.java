package test.service;

import test.service.MyService.MyBinder;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class ServiceActivity extends Activity {
	private MyService mService;
	private boolean mBound;
	
	private ServiceConnection mConnection = new ServiceConnection() {
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mBound = false;
		}
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mBound = true;
			MyBinder binder = (MyBinder)service;
			mService = binder.getService();
		}
	};
	
	public void onButtonClick(View v) {
		if(mBound) {
			int randomInt = mService.getRandomInteger();
			TextView textView = (TextView)findViewById(R.id.textView);
			textView.setText(String.valueOf(randomInt));
		} else {
			Log.d("ServiceActivity", "Not connected to a service");
		}
	}
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	
    	Intent intent = new Intent(this, MyService.class);
    	bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }
    
    @Override
    public void onStop() {
    	super.onStop();
    	
    	if(mBound) {
    		unbindService(mConnection);
    		mBound = false;
    	}
    }
}